---
title: About
subtitle: Puck Meyer  
---
![](About.jpg)

Mijn naam is Puck en ik ben een eerstejaars CMD'er!  
Ik ben zeer perfectionistisch: Ik wil graag alles perfect hebben, ook al weet ik dat dit soms niet kan.  
Ik ben creatief, hou van de kleur geel, dol op winkelen en soms schiet ik ook nog wat leuke plaatjes!  

Hier is het ontwerpproces te zien van mijn team en vooral ook de dingen die ik heb gedaan aan ons project! Ook mijn STARR(T)'s zijn hier te vinden.   
Voor mijzelf staan hier ook de hoorcollege's op, zodat ik die makkelijk terug kan lezen.  
