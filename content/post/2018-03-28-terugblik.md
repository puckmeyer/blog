---
title: Terugblik kwartaal 3
date: 2018-03-28
---

Dit kwartaal is echt voorbij gevlogen. Ik heb het idee dat ik dat bij ieder kwartaal zeg...   
In het begin van het kwartaal ging het allemaal wat moeizaam. Er kwam niks uit handen en niemand zat er echt gemotiveerd bij. Aan het einde kwam de druk wat hoger te staan, waardoor ik wel het idee had dat we meer voor elkaar kregen.    
Ik heb veel geleerd. Ik heb 6 workshops gevolgd waar ik erg blij mee ben. Ik heb hier veel kennis uit kunnen halen voor mijn project.    
Ik heb meer onderzoek gedaan dit kwartaal en dat was ook een van mijn leerdoelen. 
Ik vond dit kwartaal wel erg leuk. Ondanks we vaak niet echt motivatie hadden om iets aan het project te doen, hebben we het wel erg gezellig gehad. De sfeer was in ieder geval goed!
