---
title: Een kijkje in het ontwerpproces
subtitle: 4 September
date: 2017-09-04
tags: ["vooruitgang", "project"]
---


Vandaag hebben we gewerkt aan onze team inventarisatie. In het begin wisten we niet echt waar het goed voor was, maar uiteindelijk heeft het toch voor overzicht kunnen zorgen doordat de afspraken die binnen ons team gemaakt waren er duidelijk op stonden. 

Het was nog best lastig om uit te zoeken wat de kwaliteiten van de personen uit mijn team waren.. Je zegt toch niet snel over jezelf waar je goed in bent. Maar na een aantal minuten kwam het gelukkig wel op gang. 
Ik had een lijst opgezocht met eigenschappen en ze een voor een opgenoemd, zodat mijn teamgenoten wat sneller zouden vertellen wat wel bij hun past.

![](teaminventarisatie.jpg)


> Written with [StackEdit](https://stackedit.io/).