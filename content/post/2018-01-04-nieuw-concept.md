---
title: Ideevorming
subtitle: Nieuw Concept
date: 2018-01-04
tags: ["STARRT", "ideevorming"]
---


Situatie    
Gezien ons concept werd afgekeurd tijdens de pitch, moest er een nieuw concept bedacht worden. We hielden een brainstorm.     

Taak    
Aan mij de taak om met ideeën te komen voor een nieuw concept.     

Actie    
Tijdens de brainstorm had ik mijn laptop nog openstaan. Normaal gesproken doe ik die dicht, zodat ik mijn volledige aandacht bij de brainstorm heb. Dit keer deed ik het niet om eens te zien wat er zou gebeuren. Ik had toevallig de site van Fabrique openstaan en klikte op het kopje ‘Portfolio’. Toen zag ik een foto staan met een scherm erop die te maken had met het ov. Dit beeld inspireerde mij tot een nieuw concept. Ik heb het idee onmiddellijk aan mijn team verteld. Ik heb hier schetsen bij gemaakt om het zo te verduidelijken.    

Resultaat    
Mijn teamleden voelden gelijk de drang om verder te gaan met het uitwerken van dit concept. De brainstorm heeft dus werkelijk geleid tot een nieuw concept waar we allemaal tevreden over waren!     

Reflectie    
Normaal gesproken doe ik mijn laptop wel dicht tijdens een brainstorm. Dat zorgt bij mij voor concentratie en het ziet er ook professioneler uit. Ik ben blij dat ik mijn laptop nog open had staan en ik besloot om nog even te kijken wat Fabrique allemaal had gemaakt. Door deze manier is er een (voor ons) goed idee uit tevoorschijn gekomen.    

Transfer    
Met een volgende brainstorm is het misschien wel een optie dat een of twee uit het team op internet zitten en daar inspiratie uit opdoen. Zo kan je weer op hele andere ideeën komen.     
