---
title: Een kijkje in het ontwerpproces
subtitle: 23 Oktober
date: 2017-10-23
tags: ["vooruitgang", "project"]
---

﻿Een nieuwe week is weer aangebroken, de vakantie is voorbij en dat betekent dat we lekker aan de slag gaan.  
Ik had in de vakantie taken onderverdeeld, zodat iedereen iets kon doen in de vakantie en vandaag al een stuk verder zouden zijn.   
Jammer genoeg hadden drie personen uit mijn team hier niks aan gedaan, dus waren we alsnog heel de ochtend bezig met dit onderdeel van ons prototype.   

Ik heb in de tussentijd al mijn meubels die ik had gemaakt voor in de escape room in elkaar gezet. 

![](meubels.jpg)

Ook hebben Dominique en Robin erg veel aan hun eigen blog gewerkt vandaag. Dit vonden ik en mijn andere twee teamgenoten niet kunnen, maar hoevaak we er ook wat van zeiden: we kregen iedere keer schele gezichten en er werd totaal niet naar ons geluisterd...

> Written with [StackEdit](https://stackedit.io/).