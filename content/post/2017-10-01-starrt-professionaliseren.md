---
title: STARRT Professionaliseren
subtitle: Mail
date: 2017-10-01
tags: ["STARRT", "Professionaliseren"]
--- 


### Situatie  
Aangezien ik heel veel stress had ervaren bij de deadline van iteratie 1, was ik bang dat hetzelfde zou gaan gebeuren bij iteratie 2.

### Taak   
Mijn taak was om een mail aan mijn team te sturen en duidelijk te maken waar ik mee zit en wat ik van ze verwacht.

### Actie  
Ik ben begonnen met het opschrijven van alle punten die ik graag eens duidelijk zou willen maken. Ook de punten waarvan ik dacht dat die verbeterd konden worden heb ik aangepakt. 

### Resultaat  
De mail is goed ontvangen. Voor mijn teamgenoten was het duidelijk waar dit vandaan kwam. Jammer genoeg was D wat minder blij met de mail: ze schoot gelijk de verdediging in. Ik heb dit geprobeerd te laten voor wat het is en ben verder gegaan aan het project. 
Na de mail leek het weer even goed te gaan. De motivatie was terug en er werd keihard gewerkt! Hierdoor hadden we onze deliverables zelf een dag van tevoren af. 

### Reflectie  
Ik ben trots op mijn mail. Ik heb van tevoren goed nagedacht over wat ik er graag in wilde zetten. Het belangrijkst was natuurlijk dat de motivatie weer terug keerde.

### Transfer   
Ik heb gemerkt dat een mail heel veel kan doen met personen. Nu was de mail die ik had geschreven niet de meest positieve mail, maar toch heeft het mijn teamgenoten weer een beetje pit gegeven. 
Bij de volgende projecten zou ik wel vaker een mail willen sturen. Het liefst natuurlijk geen negatieve mail met mijn frustraties, maar een motiverende mail. Een duw in de goede richting. 

![](mail-1.jpg)
![](mail-2.jpg)

> Written with [StackEdit](https://stackedit.io/).