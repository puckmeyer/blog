---
title: Hoorcollege Verbeelden
date: 2017-09-12
tags: ["hoorcollege"]
---


Er zijn 8 competenties die aangeven waartoe wij worden opgeleid. Éen daarvan is verbeelden/ uitwerken.   
De ontwerper geeft vorm aan concepten, inzichten, ideeën en dergelijke (en werkt deze uit tot betekenisvolle, overtuigende en coherente beelden, schema's en prototypes). Een ontwerper moet iets visueel uit kunnen werken.    
**Beeld**: alle communicatiemiddelen die niet primair tekst zijn, die door middel van een tweedimensionaal medium tot ons komen en primair een communicatief-retorische functie hebben.   

Beeld is in onze context een vorm van communicatie.    
**ZBO model**: er is een boodschap (visuele uiting) van zender naar ontvanger.    

**Communicatief-retorische-functie**: overtuigen. Je wilt iemand overtuigen met beeld.    


----------


### Visuele Communicatie (Beeldtaal)   
Er zijn drie niveaus te onderscheiden.    
1. Kijken (waarnemen)   
2. Beelden begrijpen & interpreteren   
3. Beelden kiezen en maken (en daarmee overtuigen)   
Deze drie niveaus bepalen **visuele geletterdheid**: de mogelijkheid visuele boodschappen te begrijpen en te produceren.    
Je kunt dit bekijken vanuit twee perspectieven: die van de maker en de waarnemer.    

Als je van te voren iets bedenkt doe je dat meestal al schetsend. Schetsen is een vrij eenvoudige manier van verbeelden, maar wel belangrijk! Schetsen hebben 3 functies: het verbeelden van gedachten en ideeën, het uitwisselen van gedachten en ideeën en het opslaan of bewaren van gedachten en ideeën.    
Schetsen: idee-ontwikkeling (thinking sketches), overdragen/ bespreken (talking sketches), bewaren (storage).   
![](Storing.jpg)


----------
### Waarnemen   
Bij het waarnemen zijn er ook drie niveaus te onderscheiden.    

 - Zien, **Gestalt**   
 - Begrijpen, **Semiotiek**   
 - Overtuigen, **Visuele retorica**   
 
Het een kan niet zonder het ander.   


----------
### Gestalt   
Met gestalt wordt 'Het geheel' bedoeld.    
Alles wat we **zien** kun je samenvatten in de gestaltwetten.   

> 1. Wet van voorgrond en achtergrond     
> 2. Wet van eenvoud   
> 3. Wet van nabijheid  
> 4. Wet van overeenkomst   
> 5. Wet van symmetrie   
> 6. Wet van gelijke achtergrond    
> 7. Wet van geslotenheid    
> 8. Wet van ingeslotenheid   
> 9. Wet van continuïteit    
> 10. Wet van ervaring   
> 11. Wet van gelijke bestemming   
> 12. Wet van het ingevulde hiaat   


----------
### Semiotiek    
Semiotiek is de leer van de tekens. Ook hierin zijn weer drie niveaus te onderscheiden.    

- Iconische tekens: vertonen gelijkenis (overeenkomst)   
- Indexicale tekens: verwijzen naar iets (deel-geheel/ oorzaak-gevolg)   
- Symbolische tekens: tekens die op afspraken, cultuur of traditie berusten. (Je moet het weten (Metafoor)).   

Het aller bekenste symbool in het Westen is een kruis: iedereen hier symboliseert dat kruis met het Christendom.    


----------
### Retorica   
Retorica betekent overtuigen. Er zijn drie niveaus te onderscheiden.   

 - Ethos: geloofwaardigheid, eigen kwaliteiten   
 - Pathos: emotie   
 - Logos: argument (feiten, onderzoek, oorzaak-gevolg, vergelijkingen, praktische voordelen, etc.)   

----------
### Stijlmiddelen    
Stijlmiddelen versterken het beeld.   
**Regelmatigheden** (herhaling):   

 - Beeldrijm   
 - Repetitio   
 - Contrast   
 - Verbo-picturaal schema   

**Onregelmatigheden** (tropen)   

 - Metafoor   
 - Een deel van het geheel   
 - Pastiche: een beeld namaken en een nieuwe invulling geven   
 - Hyperbool: overdrijvingen    
 - Oxymoron: tegenstelling die niet klopt   
 - Personificatie  
 - Vergelijking   

> Written with [StackEdit](https://stackedit.io/).