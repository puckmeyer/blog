---
title: Inrichten Ontwerpproces
subtitle: User Journey
date: 2017-12-17
tags: ["STARRT", "ontwerpproces"]
---


Situatie   
Er moest een user journey gemaakt worden van de huidige situatie zodat we konden zien waar de eventuele ‘problemen’ zaten waardoor mensen minder snel een vervolgbezoek aan PAARD willen brengen.    

Taak   
Mijn taak was om bij de verschillende situaties de activiteiten, kansen en gedachtes op te noemen.     

Actie    
Ik heb eerst op facebook gekeken naar de recensies van de bezoekers: wat kan er beter, wat is er al goed? Dit konden we vaak ook terug laten komen bij de gedachtes. Verder heb ik ook heel erg mijn eigen perspectief gebruikt. Hoe ik mij voel bij zo’n bezoek aan een club, hoe ik er naartoe ga, wat ze voor mijn gevoel zouden kunnen veranderen (kansen). Verder ook vooral gekeken naar wat algemene activiteiten zijn bij de kopjes (van oriënteren tot na het bezoek).    

Resultaat    
Na het lezen van de recensies en die in de user journey gevoegd te hebben kregen we gelijk een indruk waar sommige gasten zich aan storen en waaraan wat te veranderen valt (de kansen).    

Reflectie    
Het maken van de User Journey is goed gegaan. We wisten na het maken van de user journey waar het bij Paard soms mis gaat en vanuit hier konden we een concept bedenken die hierop kan aansluiten. Of het helemaal aansluit weet ik niet zeker, wel dat het wachten van de rij korter is geworden.    

Transfer    
Ten eerste vind ik een User Journey heel handig en deze zal ik ook zeker in de volgende projecten gaan maken. Ik heb ook ontdekt dat het lezen van (facebook) recensies erg helpt bij het vinden van het probleem. Door de recensies werd snel duidelijk wat mensen goed en ‘slecht’ vinden. Ik ga dus in de volgende kwartalen zeker kijken of het mij dan weer helpt om recensies te lezen.    
