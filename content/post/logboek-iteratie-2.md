---
title: Logboek iteratie 2
date: 2017-09-18
tags: ["logboek"]
---


﻿18 09 17:   

> Studio dag. Alle teams uit de studio hebben hun oncept en prototype
> gepresenteerd, waarop we feedback kregen. Briefing 2 gekregen met de
> deliverables voor woensdag 4 oktober. Planning een een korte
> taakverdeling gemaakt, aangezien Robin er deze dag niet was. Aan het
> einde van de dag aan mijn blog gewerkt.

19 09 17:   

> We kregen een hoorcollege over Prototyping. Aangezien Robin en Ser uit
> mijn team ziek waren zijn we naar huis gegaan en heb ik hoofdstuk 1 en
> 2 van Beeldtaal samengevat. Ook heb ik gewerkt aan mijn blog en via de
> groepsapp hebben we taken verdeeld voor het onderzoeken van drie
> verschillende spellen.

20 09 17:  

> Studio dag. Ser en Robin waren er deze ochtend niet. Quinty, Dominique en ik hebben drie spellen getest en een spelanalyse gemaakt. In de middag hadden we studie coaching,
> wat ging over het dit-ben-ik profiel en de teamafspraken + ritueel.  

21 09 17:   

> Tools for design: Photoshop. Het werkcollege ging niet door, dus zijn Quinty, Dominique en ik naar de macdonalds gegaan om ons 'ritueel' uit te voeren.
> Hier hebben we foto's van gemaakt en ik heb de foto zo gephotoshopt dat we alle 5 op de foto staan. Ook heb ik mijn dit-ben-ik profiel gemaakt.  

22 09 17:  

> Workshop prototyping gehad. Robin en ik zijn hierna de straat op gegaan om mensen te interviewen en te weten te komen wat hun favoriete plekjes zijn in Rotterdam.  
> In de avond heb ik de teamafspraken, het ritueel en mijn dit-ben-ik profiel ingeleverd op N@tschool.  

23 09 17:  

> -

24 09 17:  

> -

25 09 17:    

> Studio dag. Ontwerpproceskaart gemaakt. Ik heb een brainstorm geleid om op ideeeën te komen voor ons nieuwe spel. We hebben drie concepten uitgewerkt en voorgelegd aan Bob.
> Door de feedback hebben we ons concept beter kunnen uitwerken. Ook heb ik een CMD student ondervraagd over hoe je een eigen stijl ontwikkeld.  

26 09 17:   

> Hoorcollege onderzoeken. Jammer genoeg waren Robin en Ser er niet vandaag. We zouden moodboards gaan maken, maar door de ontbrekende informatie van Robin en Ser zijn we naar huis gegaan.  

27 09 17:  

> Studio dag. Prototype maken van onze Escape Room. Bij studie coaching heb ik een presentatie gegeven over hoe je een eigen stijl ontwikkeld. We hebben het ook over drie van de 8 CMD Competenties gehad.  

28 09 17:  

> Tools for design: Photoshop. We hebben het prototype afgemaakt. In de middag werkcollege: we moesten Lego rangschikken. Thuis heb ik Beeldtaal hoofdstuk 3 samengevat.  

29 09 17:  

> Deze ochtend hadden we een 0-meting tekenen. In de avond met mensen van studie naar Rrrollend Rotterdam!  

30 09 17:  

> -

01 10 17:   

> Vandaag heb ik aan de deliverables gewerkt. Mail gestuurd aan team over ontevredenheid.  

02 10 17:  

> Studio dag. Alle deliverables in orde gemaakt.  

03 10 17:  

> Ik heb alle deliverables van mijn team ingeleverd. Deze dag was ik ziek, dus ik heb niet veel gedaan. Ik heb hoofdstuk 4 van Beeldtaal samengevat.  

04 10 17:  

> Presenteren voor Alumni en kijken bij de andere presentaties. In de middag hadden we studie coaching.  

05 10 17:  

> Tools for design: Photoshop. In de middag een 0-meting Engels en Nederlands.  


> Written with [StackEdit](https://stackedit.io/).