---
title: Professionaliseren
subtitle: Workshops volgen
date: 2018-01-15
tags: ["STARRT", "professionaliseren"]
---


Situatie     
Aan het begin van het kwartaal 2 is er een nieuw systeem ingevoerd zodat je je online in kan schrijven voor workshops.     

Taak    
Aan mij de taak om zelf in de gaten te houden welke workshops er welke dag waren en mij daarvoor in te schrijven.     

Actie     
Ik opende het rooster en zag de workshop HTML/CSS staan. Deze zou uit twee workshops bestaan. Ik schreef me hiervoor in. Toen Bob ziek werd, zijn twee andere studenten de workshop gaan geven. Ookal moest ik de eerste les over doen, heb ik me hier wel voor ingeschreven. De laatste twee weken was ik benieuwd naar de workshops die anderen hadden gevolgd en toen hoorde ik workshops waar ik nog nooit van had gehoord     

Resultaat    
Hartstikke goed dat ik de workshop HTML/CSS heb gevolgd, het was ook erg interessant, maar in de tussentijd heb ik er helemaal niet bij stilgestaan om me voor andere workshops in te schrijven. Ik heb dus maar één workshop gevolgd.     

Reflectie    
Ik vind het jammer van mezelf dat ik er niet eerder achterkwam dat ik me voor andere workshops in moest schrijven. Ik wist natuurlijk wel dat ik workshops moest volgen, maar door de nieuwe manier van inschrijven vergat ik gewoon te kijken naar de workshops. Ik had namelijk nog zoveel meer kunnen leren dit kwartaal.     

Transfer     
Volgend kwartaal wil ik het zeker weten anders aan gaan pakken. Ik heb al tegen Elske gezegd dat dit systeem niet voor mij is weggelegd. Ik denk dat de beste manier voor mij is om begin kwartaal 3 heel de planning van de workshops over te nemen op papier, of desnoods uit te printen. Ik vind het namelijk veel fijner om informatie via papier, iets tastbaars, op te nemen dan via internet.    
Hopelijk helpt deze manier mij volgend kwartaal met het mij inschrijven voor workshops.    