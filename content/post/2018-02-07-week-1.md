---
title: Kwartaal 3 week 1
date: 2018-02-07
tags: ["logboek"]
---

Tijdens de kick-off week zijn we met de hele studie CMD naar Maastricht afgerezen om hier een start te maken aan het nieuwe project. Iedereen kreeg een doelgroep toe gewezen, ik had 50+, waar we onderzoek naar moesten doen.
Het was hartstikke leuk, overdag was het allemaal wat serieuzer, nouja, voor zoverre je het serieus kon noemen, en in de avond ging het er wat ruiger aantoe.
Na deze twee dagen was ik helemaal brak, maar het was de moeite waard...    

Na deze week heb ik lekker uitgerust, zodat ik eerstvolgende maandag helemaal fris en fruitig aan het nieuwe project kon beginnen.     
      
      
