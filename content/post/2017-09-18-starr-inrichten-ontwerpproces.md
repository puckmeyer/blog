---
title: STARRT Inrichten Ontwerpproces
subtitle: Creatieve Techniek
date: 2017-09-18
tags: ["STARRT", "ontwerpproces"]
---

### Situatie   
We hadden net de nieuwe briefing gekregen (Kill your darling) met daarin de opdracht om een compleet nieuw concept te bedenken. Het bedenken van nieuwe ideeën ging nogal stroef, dus besloot ik een brainstormsessie te leiden, in de hoop elkaar te inspireren. 

### Taak   
Mijn taak was om iedereen die een idee had aan de beurt te laten komen zonder dat mijn teamgenoten door elkaar heen zou gaan praten. Ook het opschrijven van de ideeën zou mijn taak worden. 

### Actie   
Eerst heb ik er twee A3 vellen bij gepakt, zodat er genoeg ruimte was om de ideeën op te schrijven. Mijn team was nog een beetje afwezig en hadden hun laptops ook nog opengeklapt. Daarom besloot ik dat alle laptops tijdens de brainstormsessie dicht moesten. Zo konden we aandachtig deelnemen aan de creatieve techniek. 

Ik sprak met mijn team af om ideeën op te noemen. Aangezien de ideeën al snel op gang kwamen, begonnen we door elkaar te praten en raakte ik het overzicht kwijt. Daarom dacht ik dat het beter zou zijn als iedereen zijn hand zou opsteken als ze een idee hadden en die in de groep wilden gooien. Zo kon ik iedere keer iemand aanwijzen en zou het overzicht blijven.

### Resultaat  
We zijn uiteindelijk op aardig wat ideeën gekomen! We hebben van alle ideeën zelfs 3 nieuwe concepten kunnen samenstellen. Ook werd de orde goed bewaard door het opsteken van de handen. Zo kon iedereen rustig zijn/ haar idee aan de groep vertellen en had ik genoeg tijd om het op te schrijven.

### Reflectie   
Ik vind dat ik goed heb gehandeld. Ik ben blij dat ik snel ben overgestapt naar het opsteken van de handen. Zo ging het stukken beter. 
De resultaten waren positief en binnen een korte tijd zijn we veel ideeën rijker geworden. 
Voordat de brainstormsessie begon ging het nog een beetje stroef, maar toen we eenmaal begonnen bleven we elkaar inspireren door de ideeën van elkaar. 

### Transfer  
De volgende iteraties ga ik deze methode zeker weer gebruiken. Ik zal deze gaan gebruiken om snel op ideeën te komen en ook als de inspiratie weg is bij mijzelf en/of teamgenoten. 

![](brainstorm-2.jpg)

> Written with [StackEdit](https://stackedit.io/).