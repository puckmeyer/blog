---
title: Een kijkje in het ontwerpproces
subtitle: 24 Oktober
date: 2017-10-24
tags: ["vooruitgang", "project"]
---


﻿
Vandaag zijn we (mijn team) naar school gekomen om verder te werken aan ons project. Aangezien de secondelijm gister op was geraakt, had ik vandaag een nieuwe tube meegenomen zodat we de meubels vast konden zetten in de escape room.   
Nadat dit gebeurd was, zijn we begonnen met de spelanalyse. We hebben als eerst het spel als geheel getest en hierna hebben we al onze minigames getest. Het idee was om allemaal 1 of twee minigames uit te werken. Het leek ons leuk om er een paar door ons eigen team te laten testen: en dat is goed gelukt! Iedereen vond het leuk en er was weinig tot niks aan op te merken.  
Wel vond ik het jammer dat Dominique en Robin vandaag ook weer de gehele dag aan hun eigen blog hebben gewerkt. Ik heb hier een aantal keer wat van gezegd, maar het drong telkens maar niet tot ze door. Er was/ is nog genoeg te doen, dus jammer dat ze daar niet aan mee (willen) werken.   

Dit geeft mij veel stress... En dat weten ze. Ik heb dit al vaker aangegeven, maar het blijft gebeuren. Ik ga morgen misschien even praten met een project begeleider of mijn studie coach. 


> Written with [StackEdit](https://stackedit.io/).