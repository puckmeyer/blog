---
title: STARRT Professionaliseren
subtitle: Presentatie eigen stijl
date: 2017-09-27
tags: ["STARRT", "professionaliseren"]
---


### Situatie   
Tijdens studiecoaching moesten we samen met de klas aspecten opnoemen die wij in een dit-ben-ik profiel zouden willen zien. Ik zei toen dat het voor degenen die je dit-ben-ik profiel lezen handig is om gelijk al de eigen stijl (vormgeving van het dit-ben-ik profiel) van jou te zien. Zo kunnen ze behalve tekst ook zien wat je voorkeur is qua vormgeving. Er was nogal wat onduidelijkheid in de klas over mijn uitspraak.

### Taak  
Door de onduidelijkheid hebben mijn studie coach en ik besloten dat ik hierover de volgende les een korte presentatie zou geven. 
Ik zou een tweedejaars gaan interviewen en hem vragen hoe je een eigen stijl ontwikkeld. 

### Actie  
Om aan informatie te komen, ben ik een tweedejaars gaan interviewen. Ik vroeg hem: “Hoe ontwikkel je een eigen stijl?”. Van de antwoorden die hij gaf heb ik een mooi kloppend verhaal gemaakt. 

Dit heb ik vervolgens de week erop gepresenteerd aan de klas. 

### Resultaat  
Na mijn presentatie was het voor de klas een stuk duidelijker wat ik de les ervoor bedoelde met een eigen stijl verwerken in het dit-ben-ik profiel. Tegelijkertijd is het voor mij een goede stap geweest om beter te worden in presenteren. 

### Reflectie  
Eerst vond ik het niet zo fijn dat ik moest gaan presenteren, maar nadat ik gepresenteerd had wist ik wel waarom mijn studie coach dit van mij had gevraagd. Voor de klas was het gelijk een stuk duidelijker. Ook heb ik zelf feedback gekregen van twee klasgenoten. Zo kan ik ook aan bepaalde aspecten gaan werken, zodat het presenteren de volgende keren steeds beter gaat. 

### Transfer  
Mijn leerdoel voor het komende kwartaal is om iets zelfverzekerder over te komen tijdens het presenteren, denk aan houding, stemgebruik, gebaren. 

![](presenteren.png.jpg)

> Written with [StackEdit](https://stackedit.io/).