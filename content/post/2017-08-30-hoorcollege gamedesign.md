---
title: Hoorcollege gamedesign  
date: 2017-08-30
tags: ["hoorcollege"]
---



### Wat is een game?   

> "Play is a free activity standing quite consciously outside "ordinary"
> life as being "not serious"".

----------


### Waar bestaat een game uit?   
Een game bestaat uit drie dingen:  
 

- Doel  
- Regels  
- Keuzes

Deze drie dingen samen vormen gameplay. (Gameplay comes first!)  


----------


 
### Waarom spelen wij games?  
Het zijn ervaringen die wij mee willen maken. Zo'n ervaring moet betekenis hebben (context). Door de context kan de ervaring relevant zijn. Door de relevantie wil je de ervaring meemaken.  
"Meaningful Play" ontstaat door interactie met het systeem (de game). Dat maakt de game zo leuk.  
Meaningful Play ontstaat door merkbare resultaten uit de acties van de speler(s). Feedback van de gebruiker kan je helpen om je spel beter te maken.  
actie = reactie  


Er zijn verschillende manieren om mensen te belonen in een game:    

- Level omhoog  
- Punten scoren  
- Audio feedback  
- Visuele feedback  


----------


 
### Bartle's player types  
- Killers: zij willen onrust veroorzaken in de game.  
- Achievers: Zij willen alles verzamelen qua punten om zo de volle 100% uit de game te halen.  
- Explorers: Waar nooit iemand zou komen in een game, komen de explorers wel (de geheime plekken van de game)  
- Socializers: Gamers die graag samen willen spelen.   


----------


  
### Regels in een game  
- Regels beperken de spelers acties  
- Regels zijn expliciet en eenduidig  
- Regels behoren toe aan elke speler  
- Regels staan vast  
- Regels komen herhaaldelijk voor  
- Regels zijn bindend  
- Regels bestaan alleen in de "Magic Circle". Binnen de combinatie van tijd en plaats accepteren wij die regels. 


----------


### 10 tips voor Rapid Prototyping
- Where's the fun?  
- Kies de juiste tool  
- Fouten maken = goed  
- Meer tijd = betere kwaliteit   
- Beperk de kaders  
- Parallelle ontwikkeling  
- Fake it till you make it  
- Kill your darlings  
- Gooi alles overboord (als alles dan nog steeds werkt is het goed!)  
- Na 20% van de beschikbare doorlooptijd van het ontwikkel traject moet het eerste prototype getest worden  

Written with [StackEdit](https://stackedit.io/).