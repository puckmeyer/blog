---
title: Een kijkje in het ontwerpproces
subtitle: 15 September
date: 2017-09-15
tags: ["vooruitgang", "project"]
---


﻿Vandaag hadden we de deadline voor iteratie 1. Wat een gestress.  
Er moest nog zoveel gebeuren en als ik dit dan zei tegen mijn team leken ze er niet echt op te reageren. Hierdoor kreeg ik steeds meer stress. 
Ik heb pas hulp gekregen toen ik voor de vijfde keer vroeg op iemand mij a.u.b kon helpen.  
Ik vond dit erg jammer. Ik heb een paar keer mijn stress geuit en gevraagd om hulp en ze hielden totaal geen rekening met mij.   
Dit wil ik nog wel een keer goed bespreken met mijn team en dit ook tegen mijn studie coach zeggen.   



> Written with [StackEdit](https://stackedit.io/).