---
title: STARRT Samenwerken
subtitle: Afspraken
date: 2017-10-01
tags: ["STARRT", "samenwerken"]
---


### Situatie  
Tijdens onze teaminventarisatie hadden we afgesproken dat we het op tijd zouden aangeven als we op een bepaalde dag wat later of helemaal niet op school zouden zijn. Zo zou iedereen weten waar hij/zij aan toe was die dag. 
R was tijdens dit project door persoonlijke redenen vaak niet op school, of later. Begrijpelijk, maar het werd of heel laat, 3 uur later, pas gezegd, of ze zei helemaal niks. Ze communiceerde niet met ons waardoor ik en mijn teamgenoten niet wisten waar wij aan toe waren. 

### Taak  
Ik merkte aan mijzelf én mijn teamgenoten dat we het erg vervelend vonden dat ze ons niet op de hoogte hield van haar aan- en afwezigheid. Ik besloot om haar eerst een keer face-to-face aan te spreken op haar gedrag. Als dit niet zou helpen, via een mail. 

### Actie  
Toen R de eerstvolgende keer weer te laat kwam en dit ook niet had doorgegeven, heb ik geprobeerd om haar hierop te wijzen en ook gezegd dat wij het best rot vinden als ze ons niet op de hoogte stelt. Ze antwoordde dat ze vaak vergeet terug te appen. 
Ik begreep haar, maar vond het geen goed excuus om ons daarom bijna iedere ochtend zo in afwachtendheid te laten zitten. R zei hierop dat ze ons vanaf dat moment beter op de hoogte zou gaan houden van haar afwezigheid. 

Helaas gebeurde dit niet en heb ik mijn irritaties en frustraties hierover nog een keer geuit, maar deze keer in een uitgebreide mail.

### Resultaat  
Na deze mail heeft ze zich zeker vaker op tijd afgemeld. Er bleven wel momenten waarop het nog steeds te laat werd gezegd, maar het was na de mail in ieder geval niet meer te vergelijken met de weken ervoor. Ik heb zeker meer rust gekregen. 

### Reflectie  
Gelukkig heb ik R door mijn mail in kunnen laten zien dat het voor mij en mijn teamgenoten niet fijn was dat ze zich zo laat/nooit afmeldde. 
Als dit nog een keer zou gebeuren in een ander team, zou ik eerder zo’n mail versturen als ik zie dat het niet bij diegene doordringt. 
Mijn leerdoel voor de volgende kwartalen is dat ik sneller ergens wat van wil zeggen als ik het ergens niet mee eens ben, in plaats van het op te kroppen. 

### Transfer  
De volgende kwartalen wil ik mezelf voornemen om eerder wat tegen een teamgenoot te zeggen als iets mij stoort. 

![](mail-1.jpg)
![](mail-2.jpg)

> Written with [StackEdit](https://stackedit.io/).