---
title: Een kijkje in het ontwerpproces
subtitle: 22 Oktober
date: 2017-10-22
tags: ["vooruitgang", "project"]
---

﻿Ik had afgelopen donderdag alle meubels uit karton gesneden en die stukken heb ik vandaag geverfd. 

![](geverfd.jpg)

﻿Ik heb (zoals gewoonlijk) teveel gedaan in tegenstelling tot mijn team, kwam ik in de avond pas achter... Ik vroeg namelijk of ze het al af hadden (zodat ik wist wat ik maandag kon verwachten), maar jammer genoeg hadden 3 anderen van mijn team niks gemaakt. Ze hadden er geen tijd voor zeiden ze.   
Ik snap niet waarom dat dan niet eerder wordt aangegeven, misschien had ik dan nog wel iets kunnen doen.
Ik heb me weer eens veel te hard voor mijn team ingezet en te weinig aan mijn eigen zaakjes gedacht, zoals mijn leerdossier en blog. 


> Written with [StackEdit](https://stackedit.io/).