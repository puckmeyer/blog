---
title: Onderzoeken
subtitle: Merkanalyse concurrenten
date: 2018-01-04
tags: ["STARRT", "onderzoeken"]
---

Situatie    
Er moest een merkanalyse gemaakt worden over PAARD. Ik besloot twee merkanalyses te maken van de concurrenten van PAARD om ze zo te vergelijken.    

Taak    
Mijn taak was om informatie te verzamelen over de Grote Markt in Den Haag en Rrrollend Den Haag om zo de template van de merkanalyse in te vullen.    

Actie    
Ik ben naar de websites gegaan van de twee concurrenten en heb gezocht naar waar het ‘bedrijf’ voor staat en waar het voor wil staan. Ik heb ook bedacht wat je ruikt op bijvoorbeeld Rrrollend Den Haag. Nu helpt het dat ik wel eens naar Rrrollend Rotterdam ben geweest, dus ik heb bij deze vragen de dingen ingevuld die ik mij nog voor de geest kon halen. Nadat ik alle informatie wel zo’n beetje had verzameld heb ik de template ingevuld en meegegeven aan Mike, die ze allemaal ging laten valideren.     

Resultaat    
Ik kan na het maken van de merkanalyse zo zien wat de concurrenten wél hebben en PAARD niet. Wat het verschil is tussen de uitgaansgelegenheden.    

Reflectie    
Ik heb alle instructies gevolgd die er waren opgesteld voor het maken van een merkanalyse, maar bij het valideren werd het steeds maar niet goedgekeurd. Toen de merkanalyses bij validatie 4 eindelijk goed waren gekeurd, hebben we er jammer genoeg niet meer zoveel mee gedaan. We zaten toen al midden in het ontwerpproces.    

Transfer     
Een merkanalyse lijkt me best handig, zo kan je zien waar het bedrijf voor staat en die kenmerken ook bij de hand houden tijdens het ontwerpproces. Volgend kwartaal zou ik hier wel een workshop over willen volgen, zodat het de volgende keer hopelijk sneller gevalideerd wordt en we het dan waarschijnlijk wel bij het ontwerpen gaan gebruiken.     
