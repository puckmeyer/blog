---
title: Samenwerken
subtitle: Peerfeedback
date: 2017-12-21
tags: ["STARRT", "samenwerken"]
---

Situatie    
Ik moest mijn teamgenoten feedback geven over hoe het gaat in het team, terwijl ik het gevoel had dat ik ze nog helemaal niet zo goed kende.    

Taak    
Ik moest zonder mij schuldig te voelen zeggen wat beter kan bij sommigen van ons team en natuurlijk zeggen wat al goed ging. Ook bedenken wat de beste feedback is om te geven, want ookal kennen ik ze nog niet erg goed wil ik wel dat ze wat aan de feedback hebben.     

Actie    
Voordat ik begon met schrijven, heb ik mijn teamgenoten verteld dat ik eigenlijk nog helemaal niet goed kan beoordelen op hun bezigheden binnen het team. Mijn teamgenoten zeiden hierna precies hetzelfde. Ik zei dat ik gewoon wat algemenere punten zou opschrijven en hoe ik het op dat moment zag.     

Resultaat    
Normaal gesproken hoor je feedback te krijgen die bij elk teamlid wel verschillend is en echt gericht op die persoon. Deze keer ging dat gewoon niet, omdat ik ze nog niet heel het project mee had gemaakt. Mijn teamgenoten en ik zelf hebben dus niet een heel duidelijk beeld wat we nu precies zouden kunnen veranderen.    

Reflectie   
Uiteindelijk heb ik niet heel veel uit de feedback kunnen halen. Op ieders formulier stonden ook bijna dezelfde punten opgeschreven.    

Transfer    
De volgende keer lijkt het mij handiger als we weer peerfeedback moeten geven, dit aan het eind van het project te doen. Zo heb je elkaar heel het traject goed kunnen observeren en wordt de feedback wat gerichter ingevuld, zodat de persoon die de feedback ontvangt ook echt kan zijn wat zijn/haar sterke en ‘zwakke’ punten zijn vanuit andermans perspectief.    
