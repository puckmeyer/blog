---
title: Hoorcollege Het Ontwerpproces
date: 2017-09-05
tags: ["Hoorcollege"]
---

### Wat is design?    
Design = gebruiksvriendelijkheid, prestatie, schoonheid, luxe. Mensen denken bij het woord design aan producten met esthetische waarde. Producten worden gekoppeld aan een bepaalde levensstijl.   

> "Design is everything artificial, man-made things and systems" - Raúl.

Alles wat niet door moedernatuur is gemaakt, wordt design genoemd.    
In de literatuur bestaan verschillende definities van design.     

> "Design is to design a design to produce a design" - John Hasket

In dit citaat staan de 4 definities van design:    
1. De beroepspraktijk. Er zijn verschillende disciplines in het vak. Het staat   gekoppeld aan de behoeftes van de mens (The many and yet growing fields of design)    
2. De actie. Er moet iets gemaakt worden, of te wel, het omschrijven van de 
briefing naar een prototype   
3. Het concept. Dit is het idee achter het resultaat.     
4. Het resultaat. Deze is het meest zichtbaar.     

![](Design.jpg)

----------


Design is niet hetzelfde als kunst!   
**Kunst**    
De doelstelling is subjectief/ intern.    
Het is gemaakt voor de kijker.  (Kunst zijn de behoeftes van de kunstenaar zelf).    
**Design**    
Doelstelling is objectief/ extern.    
Het is gemaakt voor de gebruikers.    


----------


Design is ook een Ability (vaardigheid/ competentie).    
Ability: Het vermogen om een handeling bekwaam uit te voeren... Wordt veelal vergaard door ervaring.    

> "Design is the ability to imagine that-which-does-not-exist, to make it appear in concrete form as a new, purposeful addition to the real world" - Harold Nelson & Erik Stolterman    

Dus... Iedereen kan ontwerpen?    
Meneer Nigel Cross zegt dat iedereen kan ontwerpen. Change: je zoekt een vernieuwing. Je wilt een nieuwe situatie creëren, omdat de huidige situatie niet meer wenselijk is.     

> "We all design when we plan for something new to happen"- Nigel Cross    

> "Everyone designs who devises courses of actions aimed at changing existing situations into preferred ones"- Herbert Simon    

![](Ontwerpproces.jpg)


----------


### Levels of design   
Bij het ontwerpproces heb je ook levels of design    

![](Levelsofdesign.jpg)


----------


### Design is a process    
Proces: Verloop van een geleidelijke verandering beschouwd in stappen of volgorde.    
Input --> Proces --> Output    
Dit ontwerpproces ga je hanteren tijdens de design challenge, maar er zijn meerdere variaties van het ontwerpproces.     

Stanford  
 ![](Stanford.jpg)   

Ideo  
![](Ideo.jpg)



> "Design is one of the most complex ways of problem solving"- Kees Dorst    


### Design Problems     
De context, gekoppeld aan twee dingen:     
1. Je bent bezig met een toekomstige situatie en niemand kan de toekomst voorspellen. De toekomst is voortdurend in verandering.    
2. Je bent bezig met de mens en de mens kan altijd van mening veranderen. De wensen van de mensen (doelgroep) kunnen veranderen.    
De mening van de mens staat nooit stil en de toekomst dus ook niet: het is onzeker.    
Er is een onbekend heden, een onbekend proces en er is een onbekende toekomst.   Een ontwerper moet daarom altijd geduld hebben met zijn doelgroep.     
Je noemt deze problemen ook wel Wicked Problems (Design Problems): het is onduidelijk en onvoorspelbaar.    


----------


> "Design is to apply directed creativity to solve wicked problems"- Raúl    

Een probleem zit in twee situaties: er is een 'problem space' en een 'solution space'. In de problem space ontstaat het probleem en ik de solution space kan je vaak de oplossing voor het probleem vinden.    
Toch is het erg moeilijk om het probleem als geheel te bevatten en te begrijpen, dus meestal wordt het grote probleem in kleine 'subdelen' gedeeld. Zo kun aan de slag gaan met steeds een klein probleem en daar een oplossing voor vinden. Als je voor alle 'subdelen' een oplossing hebt kun je het in zijn geheel bij elkaar leggen. 
Je moet dus als ontwerper op verschillende manieren naar een probleem kijken.    

> "By trying to understand the problem and experimenting with possible solutions designing becomes a learning process"- Kees Dorst      
> 

Written with [StackEdit](https://stackedit.io/).


