---
title: Logboek kwartaal 2
date: 2017-11-14
tags: ["logboek"]
---

14 11 17   

> Vandaag was de kick-off van ons nieuwe project in PAARD (Den Haag). We hebben hier uitleg gekregen over ons nieuwe project en na de lunch hebben we een film gekeken.   

15 11 17

> We hebben groepjes gemaakt door middel van de uitkomsten van de Belbin rollen test. Hierna hebben we eigenlijk gezellig met elkaar zitten kletsen. We kenden elkaar nog niet,
dus dat was wel fijn om meer over elkaar te weten te komen. We hebben deze dag onze teamnaam (Cönnekt) bedacht, de teamafspraken en de de sowt-analyse gemaakt. Ik ben alvast begonnen aan de merkanalyse van 2 concurrenten.   

16 11 17   

> Vandaag begon mijn keuzevak Infographics.   

17 11 17  

> -    

18 11 17   

> -    

19 11 17   

> -    

20 11 17   

> Ik begon deze week als teamcaptain. Terwijl ik de teamcaptain stand-up hield, zijn mijn teamgenoten begonnen met het maken van de debrief. Hierna hebben we een planning gemaakt voor het hele project, wat aardig wat tijd in beslag nam. Ik heb de merkanalyses van de twee concurrenten van PAARD vandaag nog afgemaakt.   

21 11 17   

> Hoorcollege persoonlijke beïnvloeding.    

22 11 17   

> We zaten er deze ochtend alle 5 een beetje bij als een stel dode vogeltjes, dus zijn we begonnen met een brainstorm, om wakker te worden. We zijn begonnen met de activiteiten, kansen en gedachtes van de user journey op te schrijven. We hebben hiervoor ook facebook reviews opgezocht zodat we een goed beeld hadden wat er goed en 'slecht' was aan PAARD volgens bezoekers.
Mike zou thuis de User Journey mooi uitwerken in Illustrator.   

23 11 17   

> Keuzevak Infographics. Ik moest vijf bestaande infographics meenemen en ik moest zelf een infographic maken met 25 koffiekopjes met verschillende inhoud.    

24 11 17   

> -    

25 11 17   

> -    

26 11 17   

> -    

27 11 17   

> Deze ochtend hadden we studiecoaching. Hier hebben we onze leerdoelen moeten opstellen en we deden een test om er achter te komen welke leerstijl je hebt. Ik heb volgens de test een creatieve leerstijl. Hierna zijn Remco en ik verdergegaan met het verbeteren van de merkanalyses. Na de lunchpauze kregen we de workshop HTML/CSS. Hier leerden we hoe we een eigen site maken en op die site een scrumboard plaatsen. We maakten gebruik van het programma brackets. 

28 11 17   

> Hoorcollege emotionele beïnvloeding.   

29 11 17   

> -------------    

30 11 17   

> Keuzevak Infographics. 5 infographics meenemen, onderwerp bedenken.

01 12 17   

> -    

02 12 17   

> -    

03 12 17    

> -    

04 12 17    

> -------------    

05 12 17    

> Ik zou vandaag met mijn teamgenoten naar Den Haag gaan om mensen te interviewen, maar was deze nacht erg ziek geworden, dus bleef ik maar thuis.    

06 12 17   

> Onderzoeksreslutaten besproken. Gezien het in Den Haag niet zo lekker ging met het interviewen hebben we ook Facebook reviews opgezocht. Om 12:00 startte de pitch, die gegeven werd door Lorenzo. Ons concept werd afgekraakt. We moesten opnieuw beginnen. Vandaag hadden we geen motivatie meer, dus hebben we niets meer gedaan voor ons nieuwe concept.    

07 12 17   

> Keuzevak Infographics.    

08 12 17    

> -    

09 12 17   

> -    

10 12 17    

> -    

11 12 17     

> De dag begon met studiecoaching. We kregen uitleg over het dit-ben-ik profiel die we aankomende vrijdag ik moeten leveren. Hierna zijn we verder gegaan met het bedenken van een nieuw concept. We kwamen vrij snel op een nieuw concept, maar goed ook, want vanaf 12:00 ging de school sluiten ivm de sneeuw.     

12 12 17   

> Hoorcollege Sociale beïnvloeding.    

13 12 17   

> We hadden afgesproken allemaal een low-fid prototype te maken (kleine schetsjes). Op school hebben we die met elkaar vergeleken en samengevoegd tot een geheel. We zijn in de middag begonnen aan de recap.   

14 12 17    

> Keuzevak Infographics.    

15 12 17    

> -    

16 12 17    

> -   

17 12 17    

> -    

18 12 17    

> In de ochtend kregen we weer studiecoaching. We moesten peerfeedback aan elkaar geven. Ik vond at ik nog niet zoveel kon vertellen over mijn team, dus heb wat algemene punten opgeschreven. Na studiecoaching is het testplan aangepast en zijn we naar buiten gegaan om ons concept te laten testen. Helaas snapten niet veel mensen wat het inhield, omdat het er zo strak en algemeen uitzag. Remco en ik zijn deze middag naar de validatie gegaan van het low-fid prototype.    

19 12 17   

> We zaten netjes allemaal klaar voor het hoorcollege, maar hij was verzet, zonder dat wij het wisten! We konden dus weer allemaal naar huis..    

20 12 17    

> Na de validatie van het low-fid prototype bedachten we een andere manier om te testen. We hebben allemaal verschillende pakkende zinnen bedacht en daaruit de leukste, aantrekkelijkste gekozen. Mike en ik zijn hierna aan de start gegaan met het vormgeven van de beginschermen met de teksten erop. In de avond/nacht het schoolfeest!    

21 12 17    
Eigenlijk had ik mijn keuzevak deze ochtend, maar gezien ik heel de nacht doorgefeest had, en ik pas half 8 in bed lag, leek het mij eem goed idee om maar niet te gaan ;)     

22 12 17   

> -    

23 12 17 - 07 01 18    

> VAKANTIE!    

08 01 18    

> Het was weer even inkomen vandaag, maar we hebben zeker niet bij de pakken neer gezeten. Ik heb eerst even alles wat nog gedaan moest worden op papier gezet, zodat het in een oogopslag duidelijk was wat er nog moest gebeuren. Ik ben samen met Remco en Demi begonnen met het uitwerken van de schermen van ons prototype.    

09 01 18   

> Hoorcollege visuele beïnvloeding. We hebben hierna besloten om naar huis te gaan, zodat we kunnen leren voor het tentamen aankomende donderdag.    

10 01 18   

> Lekker dagje! Mijn team en ik zijn vandaag erg productief geweest. Remco, Mike, Demi en ik zijn verder gegaan met het uitwerken van de schermen van ons prototype. Ook ben ik begonnen met het maken van de Recap.    

11 01 18   

> Keuzevak Infographics. Hierna hebben we het tentamen Design Theory 2 gehad. Ik ben vanaag weer verder gegaan met het maken van de recap.    

12 01 18    

> -     

13 01 18    

> STARRT's geschreven voor in mijn leerdossier.    

14 01 18   

> Veel aan mijn leerdossier gewerkt. De recap ook afgemaakt, zodat ik die morgen bij Elske kan laten valideren.    

15 01 18   

> De Recap heb ik vandaag laten valideren. Hij is goed gekeurd gelukkig! Ik heb vandaag samen met Remco de conceptposter gemaakt. Mijn teamgenoten hebben uit hout kleine figuurtjes laten maken die ons concept erg goed weergeven. Deze heb ik vervolgens zwart gemaakt met stift.    

16 01 18    

> - 

17 01 18   

> Vandaag is het de dag van de Expo! Spannend... Lorenzo en ik zouden vandaag gaan pitchen. We hadden vrijwel allesal af deze dag, dus we hebben niet erg veel gedaan die middag op school. 5 uur zijn we wat gaan eten bij Happy Italy en toen we terug kwamen begon het! Ik heb mijn pitch laten valideren bij Elske. Rond 9 uur ging ik naar huis.    

18 01 18    

> Keuzevak Infographics. Ook een persoonlijk gesprek met onze 'studiecoach'. 

19 01 18    

> Inleveren die handel!    

