---
title: Reflectie
subtitle: Kwartaal 4
date: 2018-06-02
---

Dit is als het goed is alweer het einde van kwartaal 4 en daarbij ook het gehele jaar. Wat is het snel gegaan. Ik heb veel geleerd, maar ik heb ook nog zeker dingen die ik wil leren, zoals het verdiepen in vormgeven en het maken van website's en apps. 
Ik heb veel geleerd over de beroepsproducten en waar deze goed voor zijn. In kwartaal 1 vond ik het maar onnodig en snapte ik niet waar het allemaal goed voor was, maar zo langzamerhand begon ikdoor te krijgen waar het allemaal voor was.     
Ik vind het jammer dat ik dit kwartaal zo weinig heb geleerd over het vormgeven. Misschien kan hier volgend jaar wat meer les of workshops aan worden besteed.     