---
title: Hoorcollege Prototyping
date: 2017-09-19
tags: ["hoorcollege"]
---

Nature vs Nurture   
Omgevingsfactoren spelen een grote rol in bijv. de opvoeding.    


----------
Je hebt een probleem en je zoekt een oplossing. Ga **niet** gelijk van het probleem naar een oplossing. Zo krijg je namelijk design fails. Je komt er vaak pas achter hoe je concept werkt als er een prototype van is gemaakt. Maak dus een andere vraagstelling voor het probleem (Omdenken).    
Is het probleem iets anders? Kan ik het op een andere manier doen?    


----------
### Introspection illusion    
**Introspection illusion**: zelfreflectie illusie (ik denk dat ik weet wat ik mooi vind (emotie)).   
Emotie en rationale gaan niet altijd samen.    

 - Een oplossing moet relevant zijn      
   Is dit wat de gebruikers willen?    
 - Is het ontwerp passend? Is het geschikt om gebruikt te worden?    

![](Beedtediepte.jpg)  


----------
### The double diamond    
The double diamond is een methode om in te zetten zodat je de juiste vraag beantwoord en het juiste probleem oplost.    

![](Doublediamond.jpg)

"4" Stappen double diamond   

 1. Onderzoeksfase    
 2. Beste kiezen    
 3. Ontwikkelen    
 4. Welke uitwerken tot eindproducten    


----------
### Minimal viable prototype
Hiermee wordt bedoeld een prototype die doet wat het moet doen en meer niet!  
Wat is de doelgroep? Welke omgeving?   
Je kunt hier ook de MOSCOW methode gebruiken.    
**MOSCOW** methode: Must have/ Should have/ Could have/ Won't have.   

 - Must haves: Wat erin moet!    
 - Should haves: Leuk als het erin zit       
 - Could haves: Het is gaaf als het erin zit, maar hoeft niet, geen zin in.    
 - Won't have: Doet afbreuk in je spel   


----------
### Breedte en diepte    
Breedte geeft een antwoord op je vraag.     
Diepte geeft een benadering op je antwoord/ eindproduct.     


----------
### Hoe haal je het meeste uit een prototype

 - Testplan opstellen: juiste vragen stellen   
 - Test objectief registreren: doorvragen! Vragen en kijken!   
 - Uitkomsten interpreteren: wat doe je ermee?    
 - Verassingen toelaten: reken op verassingen! Dingen die mislukken? Goed!   
 - Geprioriteerde lijst van actiepunten voor je concept/ proces    

Kijk naar de aspecten van je game! Hoe maak je het beter?    


----------
### Verschillende prototypes   
**Paper prototype**: van papier    
**Visual prototype**: (prune) digitaal (interactief), wees wel bezig met the right thing!      
**Wireframe prototype**: Een low fidelity prototype dat een kosten- en tijdeffectief middel is. Heel simpel, vaak grijs, zonder poespas     
**Scenario prototype**: neem mensen mee in een verhaal   

> "Failure is succes in process" - Albert Einstein

Falen is een onderdeel van je succes!   
> Written with [StackEdit](https://stackedit.io/).