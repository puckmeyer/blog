---
title: Terugblik op kwartaal 2
date: 2018-01-19   
---

Daar is het einde alweer van kwartaal 2 en wat is het weer snel gegaan. 
Afgelopen kwartaal heb ik iets rustiger aan gedaan. Ik heb niet heel erg op de deliverables gezeten. Nu kon dat ook niet erg goed met de validaties, dus we liepen al achter...    
Ik vond het een rommelig kwartaal. Ik weet niet of het aan mij ligt, maar het nieuwe systeem is niet voor mij weggelegd. Kwartaal 1 vond ik veel fijner werken. Ik moet volgend kwartaal dus wel opzoek naar een manier waardoor ik de planning goed in de gaten kan houden en de validatie en workshop momenten.    

Ik heb dit kwartaal veel geleerd van de Hoorcollege's. Ik vind psychologie erg interessant en het ging hier dan ook grotendeels over. Beroepsproducten die we dit kwartaal moesten maken die vorig kwartaal niet ter sprake kwamen, hebben mij ook erg geholpen met het 'als een geheel zien' van mijn project.   
In kwartaal 1 deed heel mijn team de opdrachten eigenlijk samen, waardoor we wisten waar we waren en waar het over ging. Dit kwartaal hebben we veel beroepsproducten verdeeld over de teamleden, dus op een gegeven moment wist ik niet meer waar we nu precies waren. De recap die ik maakte liet mij bijvoorbeeld inzien waar wij nu eigenlijk de afgelopen weken als team mee bezig zijn geweest. Dat was erg interessant om te zien.   

Zoals ik al zei: ik moet een manier vinden om zo op de hoogte te blijven van (vooral) de workshops. Ik heb dit kwartaal alleen de workshop HTML/CSS gevolgd, waar ik erg van baal. Ik had veel meer kunnen leren. 

Voor volgend kwartaal wil ik in het begin kijken welke beroepsproducten bij welke competentie staan. Dit kwartaal heb ik dit namelijk pas achteraf gedaan. Ik kreeg gister te horen van mijn studiecoach dat het handig is dit te doen, zodat je je kan focussen op die beroepsproducten en misschien wel op een voor jou gesteld leerdoel. 

Al met al was het ook een erg gezellig kwartaal. Ik ben/ was erg tevreden met mijn team! We konden goed samenwerken en hier en daar een grapje kon er ook wel in. De pauzes waren ook erg gezellig. Waar ik zag dat de groepjes van kwartaal 1 in de pauze weer naar elkaar toe trokken, bleven wij vaak met elkaar pauze houden.. Kijk, dat zegt wat!
