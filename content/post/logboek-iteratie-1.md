---
title: Logboek iteratie 1
date: 2017-08-30
tags: ["logboek"]
---


﻿30 08 17:   

> Informatie gekregen over het eerste studiejaar, met ons team gewerkt
> aan een teamnaam en een object gemaakt die ons team representeert.
> 
> 

31 08 17:   

> Informatie gekregen over hoe je ontwerpt (de richtlijnen),
> ook de opdracht te horen gekregen waar we het aankomende kwartaal mee
> aan de slag gaan. We kregen een hoorcollege over Games.

01 09 17:   

> Vrije dag

02 09 17:  

>  -

03 09 17:  

> -

04 09 17:   

> Studio dag. De teamcaptain is aangewezen (ik), er is een planning
> gemaakt (wat en wanneer af), teaminventarisatie, de taken zijn
> verdeeld voor de aankomende 2 weken.

05 09 17:   

> We kregen een hoorcollege over Design Theorie.

06 09 17:   

> Studio dag. Onderzoek gedaan, iedereen heeft individueel moodboards
> gemaakt. Workshop Blog maken. Studie coaching.

07 09 17:   

> Werkcollege. De theorie van het hoorcollege gebruiken in een
> voorbeeld. Het ging over de wicked problems: problemen die onduidelijk
> en onvoorspelbaar zijn.

08 09 17:   

> Vrije dag

09 09 17:   

> -

10 09 17:   

> Aan mijn blog gewerkt. Onderzoek afronden en spel elementen op papier
> schrijven voor maandag.

11 09 17:   

> Studio dag. Iedereen had een spel ontworpen. Vandaag hebben we alle
> spellen bij elkaar gelegd en elementen van alle spellen samengevoegd
> tot een spel. We zijn begonnen met het maken van een paperprototype.

12 09 17:   

> We kregen een hoorcollege over Verbeelden. Na het hoorcollege zijn 4
> van ons team verder gaan werken aan het paperprototype van ons spel.

13 09 17:   

> Studio dag. Aan ons paperprototype gewerkt. Workshop IDEO Prototyping
> (waar ik totaal iets anders van had verwacht…) en studie coaching.

14 09 17:   

> Tools for design: Photoshop. Ons paperprototype afgemaakt, spelanalyse
> en de deliverables in orde maken.

15 09 17:    

> Stress. Vandaag was de deadline voor iteratie 1 en er moest nog aardig
> wat gebeuren. Ik had veel te veel taken op me genomen. Ik dacht dat
> alles al af was en alleen nog maar ingeleverd hoefde te worden, was
> niet het geval. Had niet veel steun aan mijn team. Hier wil ik nog
> over gaan praten met mijn studie coach.

16 09 17:   

> Photoshop opdrachten afmaken van de les en de opdracht van thuis voor
> aankomende donderdag.

17 09 17:   

> Presentatie voorbereiden.

18 09 17:   

> Studio dag. Alle teams uit de studio hebben hun oncept en prototype
> gepresenteerd, waarop we feedback kregen. Briefing 2 gekregen met de
> deliverables voor woensdag 4 oktober. Planning een een korte
> taakverdeling gemaakt, aangezien Robin er deze dag niet was. Aan het
> einde van de dag aan mijn blog gewerkt.


> Written with [StackEdit](https://stackedit.io/).