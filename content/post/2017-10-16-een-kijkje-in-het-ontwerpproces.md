---
title: Een kijkje in het ontwerpproces
subtitle: 16 Oktober
date: 2017-10-16
tags: ["vooruitgang", "project"]
---

De eerste vakantie dag, ook al voelde dat niet zo... We hadden namelijk afgesproken om samen aan het project te gaan werken.
We waren voor de vakantie op het idee gekomen om een filmpje op te nemen als onderdeel van onze pitch (tijdens de expo).
In dit filmpje wilden we een klein verhaal vertellen waarom het spel gespeeld zou gaan worden, dus eigenlijk het verhaal achter het spel. 
We zijn dus rond een uur of 10 bijeen gekomen bij Robin thuis. Het plan was dat ik op het filmpje te zien was en die ook tegelijkertijd op zou gaan nemen. 
Voordat het filmpje opgenomen werd hebben we met het hele team de tekst verzonnen en er een goed lopend verhaal van gemaakt. 
Hierna ben ik aan de slag gegaan!   

Het duurde best lang voordat het filmpje klaar was, ik vond het namelijk moeilijker dan verwacht. 
Aangezien de anderen van mijn team toch aan het wachten waren op mij, zei ik tegen ze dat ze de afmetingen van de meubels van de escape room anders alvast konden opmeten. 
Jammer genoeg werd er vanuit hun geen actie ondernomen en hebben ze de gehele dag zitten niksen. 
Er had in de tussentijd dat ik zat te filmen zoveel door hun gedaan kunnen worden, dus dat vond ik erg jammer. We hebben namelijk nog ZAT te doen!

Link:
https://www.youtube.com/watch?v=1oqpiG-lTUA

> Written with [StackEdit](https://stackedit.io/).