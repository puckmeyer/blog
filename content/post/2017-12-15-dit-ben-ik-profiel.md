---
title: Professionaliseren
subtitle: Dit-ben-ik profiel
date: 2017-12-15
tags: ["Professionaliseren", "STARRT"]
---


Situatie     
Tijdens studiecoaching werd er aan ons gevraad om een dit-ben-ik profiel op te stellen.     

Taak     
Mijn taak was om eens goed naar mezelf te kijken. Om erachter te komen wie ik nu eigenlijk ben, wat ik al kan en wat ik dit kwartaal wil gaan leren. Ook bedenken hoe ik mijn dit-ben-ik profiel wil vormgeven.    

Actie     
Ik ben begonnen met het bedenken van mijn eigenschappen. Ik heb dit ook aan mijn moeder gevraagd om er zo achter te komen hoe iemand anders (wél iemand die mij goed kent) over mij denkt. Ook heb ik gekeken wat ik bij mijn (vorige) banen heb geleerd en of dat aansluit op mijn studie. Qua het beheersen van creatieve programma’s heb ik nog niet veel ervaring, dus daar kan ik nog wat mee (leerdoelen)! Mijn leerdoelen had ik begin dit kwartaal al opgesteld, dus die kon ik zo overnemen. Ik heb nog een SWOT-analyse gemaakt, om zo nog concreter naar mezelf te kunnen kijken (denk aan weaknesses en opportunities). Ook moest ik twee tests doen, waardoor ik kon inzien welke rol ik, waarschijnlijk, heb binnen het team. Toen ik alle punten had die ik wou gaan gebruiken in mijn dit-ben-ik profiel, ben ik gaan nadenken over de stijl voor mijn profiel. Ik heb google geopent en ben opzoek gegaan naar creatieve cv’s. Hier heb ik mijn inspiratie vandaan gehaald voor de vormgeving van mijn eigen dit-ben-ik profiel. Ik besloot mijn profiel met photoshop te maken en heb hier vervolgens al het bovenstaande ingezet. Als laatst heb ik een klein stukje reflectie geschreven waarbij ik terug kijk op vorig kwartaal.      

Resultaat     
Gezien ik nog maar in kwartaal 2 zit van jaar 1 en ik hiervoor ook geen andre creatieve opleiding heb gedaan, is mijn dit-ben-ik profiel nog best kaal en onorigineel. Het is wel fijn om het bestandje te hebben, want ik kan hem erg makkelijk aanpassen als ik wat heb geleerd. Ik kan nu ook precies zien wat ik al kan en wat niet en zo kan ik ook kijken wat ik wil leren.       

Reflectie     
Ik vond het erg interessant om naar de vaardigheden te kijken die je tijdens je werk leert. Zo kijk je ook gelijk op een hele andere manier naar dit soort dingen. Mijn dit-ben-ik profiel is nog zeker niet af, maar ik vond het onbelangrijk om er dingen in te zetten die niet van toegevoegde waarde zijn.       

Transfer     
Ik wil het de aankomende keren wel anders aanpakken. Nu keek ik pas midden in het kwartaal naar waar ik stond en wat ik wilde gaan leren. Volgend kwartaal wil ik dit gelijk aan het begin doen, dan kan ik ook gelijk mijn leerdoelen stellen.

